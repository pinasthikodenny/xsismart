﻿
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using XsisMart.Models;
using XsisMart.Utils;
using XsisMart.ViewModels;

namespace XsisMart.Controllers
{
    public class HomeController : Controller
    {
        private readonly XsisMartContextModel _context;

        public HomeController(XsisMartContextModel context)
        {
            _context = context;
        }
        public async Task<IActionResult> Index(string sortOrder,
            string currentFilter,
            string searchString,
            int? pageNumber, int? pSize)
        {
            var students = from p in _context.Products
                           join v in _context.Variants
                           on p.IdVariant equals v.IdVariant
                           join c in _context.Categories
                           on v.IdCategory equals c.IdCategory
                           select new VMProduct
                           {
                               IdProduct = p.IdProduct,
                               NameProduct = p.NameProduct,
                               Price = p.Price,
                               Stock = p.Stock,
                               NameCategory = c.NameCategory,
                               NameVariant = v.NameVariant,
                               Image = p.Image
                           }
                                          ;

            ViewData["CurrentSort"] = sortOrder;
            ViewData["CurrentpSize"] = pSize;
            ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewData["PriceSortParm"] = sortOrder == "Price" ? "price_desc" : "Price";
            ViewData["CatSortParm"] = sortOrder == "Cate" ? "Cate_desc" : "Cate";
            ViewData["VarSortParm"] = sortOrder == "Var" ? "Var_desc" : "Var";
            ViewBag.PageSize = PaginatedList<VMProduct>.PageSizeList();

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }


            ViewData["CurrentFilter"] = searchString;

            //var students = from s in list
            //               select s;
            if (!String.IsNullOrEmpty(searchString))
            {
                students = students.Where(s => s.NameProduct.Contains(searchString)
                                       || s.NameCategory.Contains(searchString)
                                       || s.NameVariant.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "name_desc":
                    students = students.OrderByDescending(s => s.NameProduct);
                    break;
                case "Price":
                    students = students.OrderBy(s => s.Price);
                    break;
                case "price_desc":
                    students = students.OrderByDescending(s => s.Price);
                    break;
                case "Cate":
                    students = students.OrderBy(s => s.NameCategory);
                    break;
                case "Cate_desc":
                    students = students.OrderByDescending(s => s.NameCategory);
                    break;
                case "Var":
                    students = students.OrderBy(s => s.NameVariant);
                    break;
                case "Var_desc":
                    students = students.OrderByDescending(s => s.NameVariant);
                    break;
                default:
                    students = students.OrderBy(s => s.NameProduct);
                    break;
            }
            int pageSize = pSize ?? 5;
            Countcart();
            return View(await PaginatedList<VMProduct>.CreateAsync(students, pageNumber ?? 1, pageSize));
            //return View(students);
        }
        public IActionResult Create()
        {
            List<Category> cd = new List<Category>();
            cd = (from c in _context.Categories select c).ToList();
            cd.Insert(0, new Category { IdCategory = 0, NameCategory = "--Select Category Name--", Description = "" });
            ViewBag.klm = cd;

            //List<Category>ListCategory = await DataCategory();
            List<Variant> cl = new List<Variant>();
            cl.Insert(0, new Variant { IdVariant = 0, NameVariant = "--Select Variant Name--", Description = "", IsDelete = false, IdCategory = 0 });

            //ViewBag.abc = ListCategory;
            ViewBag.ListVariant = cl;

            return View();

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(VMProduct _product)
        {

            if (ModelState.IsValid)
            {
                Product prodNew = new Product();
                prodNew.NameProduct = _product.NameProduct;
                prodNew.Stock = _product.Stock;
                prodNew.Price = _product.Price;
                prodNew.IdVariant = _product.IdVariant;

                _context.Add(prodNew);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            List<Category> cd = new List<Category>();
            cd = (from c in _context.Categories select c).ToList();
            cd.Insert(0, new Category { IdCategory = 0, NameCategory = "--Select Category Name--", Description = "" });
            ViewBag.klm = cd;

            //List<Category>ListCategory = await DataCategory();
            List<Variant> cl = new List<Variant>();
            cl.Insert(0, new Variant { IdVariant = 0, NameVariant = "--Select Variant Name--", Description = "", IsDelete = false, IdCategory = 0 });

            //ViewBag.abc = ListCategory;
            ViewBag.ListVariant = cl;


            return View(_product);
        }
        public async Task<IActionResult> Detail(int _IdProduct)
        {
            VMProduct list = await (from p in _context.Products
                                    join v in _context.Variants
                                    on p.IdVariant equals v.IdVariant
                                    join c in _context.Categories
                                    on v.IdCategory equals c.IdCategory
                                    select new VMProduct
                                    {
                                        IdProduct = p.IdProduct,
                                        NameProduct = p.NameProduct,
                                        Price = p.Price,
                                        Stock = p.Stock,
                                        NameCategory = c.NameCategory,
                                        NameVariant = v.NameVariant,
                                        Image = p.Image
                                    }
                                          ).FirstOrDefaultAsync(h => h.IdProduct == _IdProduct);
            if (_IdProduct == null)
            {
                return NotFound();
            }
            Product product = await _context.Products.FirstOrDefaultAsync(h => h.IdProduct == _IdProduct);
            if (product == null)
            {
                return NotFound();
            }
            return View(list);
        }
        public async Task<IActionResult> Edit(int id)
        {
            List<Category> cd = new List<Category>();
            cd = (from c in _context.Categories select c).ToList();
            cd.Insert(0, new Category { IdCategory = 0, NameCategory = "--Select Category Name--", Description = "" });
            ViewBag.klm = cd;



            List<Variant> cl = new List<Variant>();
            cl = (from d in _context.Variants select d).ToList();
            cl.Insert(0, new Variant { IdVariant = 0, NameVariant = "--Select Variant Name--", Description = "", IsDelete = false, IdCategory = 0 });

            ViewBag.ListVariant = cl;

            //ViewBag.klm = await DataCategory();


            if (id == null)
            {
                return NotFound();
            }
            VMTransaction product = await DataPM(id);//buat dapetin data produk(make VM karena butuh joinan kalo gapake pakenya model)

            //var product = await _context.Products.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }
            ViewBag.ListVariant = await DataVariant(product.IdCategory);
            return View(product);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Product _product)
        {
            if (ModelState.IsValid)
            {
                Product prodNew = new Product();
                prodNew.NameProduct = _product.NameProduct;
                prodNew.Stock = _product.Stock;
                prodNew.Price = _product.Price;
                prodNew.IdVariant = _product.IdVariant;

                _context.Update(_product);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            List<Category> cd = new List<Category>();
            cd = (from c in _context.Categories select c).ToList();
            cd.Insert(0, new Category { IdCategory = 0, NameCategory = "--Select Category Name--", Description = "" });
            ViewBag.klm = cd;

            //List<Category>ListCategory = await DataCategory();
            List<Variant> cl = new List<Variant>();
            cl.Insert(0, new Variant { IdVariant = 0, NameVariant = "--Select Variant Name--", Description = "", IsDelete = false, IdCategory = 0 });

            //ViewBag.abc = ListCategory;
            ViewBag.ListVariant = cl;
            Countcart();
            return View(_product);
        }

        public async Task<IActionResult> Delete(int id)
        {
            if (id == null)
            {
                return NotFound();
            }
            VMTransaction mnm = await DataPM(id);
            return View(mnm);
        }
        [HttpPost]
        public async Task<IActionResult> Delete(OrderDetail klm)
        {
            if (ModelState.IsValid)
            {
                _context.Remove(klm);
                await _context.SaveChangesAsync();
                return RedirectToAction("ListCart");
            }
            return View(klm);
        }
        public async Task<List<Category>> DataCategory()
        {
            List<Category> cd = await _context.Categories.ToListAsync();
            cd.Insert(0, new Category { IdCategory = 0, NameCategory = "--Select Category Name--" });
            ViewBag.klm = cd;
            return cd;
        }
        public async Task<List<Variant>> DataVariant(int IdCategory)
        {
            List<Variant> list = await _context.Variants.Where(a => a.IdCategory.Equals(IdCategory)).ToListAsync();
            list.Insert(0, new Variant { IdVariant = 0, NameVariant = "--Select variant Name--" });
            return list;
        }
        public async Task<JsonResult> GetDataVariantAjax(int IdCategory)
        {
            List<Variant> list = await DataVariant(IdCategory);
            return Json(list);
        }
        public async Task<VMTransaction> DataPM(int id)
        {
            VMTransaction list = await (from head in _context.OrderHeader
                                        join detail in _context.OrderDetails
                                        on head.IdHeader equals detail.IdHeader
                                        join prod in _context.Products
                                        on detail.IdProduct equals prod.IdProduct
                                        join abc in _context.Variants
                                        on prod.IdVariant equals abc.IdVariant
                                        join cate in _context.Categories
                                        on abc.IdCategory equals cate.IdCategory
                                        where detail.IdDetail == id && head.IsCheckout == false
                                        select new VMTransaction
                                        {
                                            IdProduct = prod.IdProduct,
                                            Amount = detail.Amount,
                                            NameProduct = prod.NameProduct,
                                            NameCategory = cate.NameCategory,
                                            NameVariant = abc.NameVariant,
                                            IdCategory = cate.IdCategory,
                                            IdVariant = abc.IdVariant,
                                            IdCustomer = head.IdCustomer,
                                            IdDetail = detail.IdDetail,
                                            IdHeader = head.IdHeader,
                                            Price = prod.Price,
                                            CodeTransaction = head.CodeTransaction,
                                            IsCheckout = head.IsCheckout,
                                            Quantity = detail.Quantity

                                        }).FirstOrDefaultAsync();
            return list;
        }


        public int Countcart()
        {
            int count = _context.OrderDetail.Where(a=>a.IsDelete==false).Count();            
            ViewBag.count = count;
            return count;
        }
        public async Task<IActionResult> ListCart()
        {
            int IdCust = 1;
            List<VMTransaction> list = await DataTransaction(IdCust);
            DateTime date = DateTime.Now;
            decimal subtotal = await dataSumOrder(IdCust);

            double tax = ((double)5 / 100) * ((double)subtotal);
            ViewBag.DateNow = date;
            ViewBag.SubTotal = subtotal;
            ViewBag.Tax = tax;
            ViewBag.Total = ((double)subtotal + tax);
            ViewBag.CodeTransaction = list.Select(a => a.CodeTransaction).FirstOrDefault();
            ViewBag.IdHeader = list.Select(a => a.IdHeader).FirstOrDefault();
            ViewBag.IsCheckout = list.Select(a => a.IsCheckout).FirstOrDefault();
            ViewBag.IdCustomer = list.Select(a => a.IdCustomer).FirstOrDefault();
            return View(list);



        }
        public async Task<List<VMTransaction>> DataTransaction(int IdCustomer)
        {
            List<VMTransaction> list = await (from head in _context.OrderHeader
                                              join detail in _context.OrderDetails
                                              on head.IdHeader equals detail.IdHeader
                                              join prod in _context.Products
                                              on detail.IdProduct equals prod.IdProduct
                                              join abc in _context.Variants
                                              on prod.IdVariant equals abc.IdVariant
                                              join cate in _context.Categories
                                              on abc.IdCategory equals cate.IdCategory
                                              where head.IdCustomer == IdCustomer && head.IsCheckout == false
                                              select new VMTransaction
                                              {
                                                  IdProduct = prod.IdProduct,
                                                  Amount = detail.Amount,
                                                  NameProduct = prod.NameProduct,
                                                  NameCategory = cate.NameCategory,
                                                  NameVariant = abc.NameVariant,
                                                  IdCategory = cate.IdCategory,
                                                  IdVariant = abc.IdVariant,

                                                  IdCustomer = head.IdCustomer,
                                                  IdDetail = detail.IdDetail,
                                                  IdHeader = head.IdHeader,
                                                  Price = prod.Price,
                                                  CodeTransaction = head.CodeTransaction,
                                                  IsCheckout = head.IsCheckout,
                                                  Quantity = detail.Quantity
                                                 


                                              }).ToListAsync();

            return list;
        }
        public async Task<decimal> dataSumOrder(int IdCustomer)
        {
            decimal subtotal = 0;

            subtotal = await (from head in _context.OrderHeader
                              join detail in _context.OrderDetails
                              on head.IdHeader equals detail.IdHeader
                              where head.IdCustomer == IdCustomer && head.IsCheckout == false
                              select detail.Amount).SumAsync();
            return subtotal;
        }

        [HttpPost]
        public async Task<IActionResult> Update(VMTransaction _product)
        {

                List<OrderDetail> xyz = await DataMM(_product.IdHeader);
                OrderHeader mnz = await _context.OrderHeader.Where(a => a.IdCustomer == _product.IdCustomer && a.IdHeader.Equals(_product.IdHeader)&&a.IsCheckout==_product.IsCheckout).FirstOrDefaultAsync();
                try
                {
                    mnz.IsCheckout = true;
                    int qwerty = xyz.Count();
                    for (int i = 0; i < qwerty; i++)
                    {
                        xyz[i].IsDelete = true;
                        _context.Update(xyz[i]);
                    }
                    _context.Update(mnz);
                    await _context.SaveChangesAsync();
                    return Json(new { status = "success" });
                }
                catch (Exception e)
                {
                    return Json(new { status = "error", messages=e.Message.ToString() });
                    throw;
                }
        }
        public async Task<List<VMTransaction>> DataJM(int IdHeader)
        {
            List<VMTransaction> list = await (from head in _context.OrderHeader
                                              join detail in _context.OrderDetails
                                              on head.IdHeader equals detail.IdHeader                                           
                                              where detail.IdHeader == IdHeader && head.IdHeader == IdHeader
                                              select new VMTransaction
                                              {
                                                  Amount = detail.Amount,
                                                  IdCustomer = head.IdCustomer,
                                                  IdDetail = detail.IdDetail,
                                                  IdHeader = head.IdHeader,
                                                  CodeTransaction = head.CodeTransaction,
                                                  IsCheckout = head.IsCheckout,
                                                  Quantity = detail.Quantity



                                              }).ToListAsync();

            return list;
        }
        public async Task<List<OrderDetail>> DataMM(int IdHeader)
        {
            List<OrderDetail> list = await _context.OrderDetail.Where(a => a.IsDelete == false && a.IdHeader.Equals(IdHeader)).ToListAsync();
            return list;
        }
    }
}
