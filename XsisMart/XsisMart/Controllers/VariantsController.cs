﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using XsisMart.Models;
using XsisMart.Utils;
using XsisMart.ViewModels;

namespace XsisMart.Controllers
{
    public class VariantsController : Controller
    {
        private readonly XsisMartContextModel _context;

        public VariantsController(XsisMartContextModel context)
        {
            _context = context;
        }

        // GET: Variants
        public async Task<IActionResult> Index(string sortOrder,
            string currentFilter,
            string searchString,
            int? pageNumber)
        {
            var students = from v in _context.Variants
                                          join c in _context.Categories
                                          on v.IdCategory equals c.IdCategory
                                          where v.IsDelete .Equals( false)
                                          select new VMVariant
                                          {
                                              IdVariant = v.IdVariant,
                                              NameVariant = v.NameVariant,
                                              Description = v.Description,
                                              NameCategory = c.NameCategory
                                          };
            ViewData["CurrentSort"] = sortOrder;
            ViewData["VarSortParm"] = String.IsNullOrEmpty(sortOrder) ? "Var_desc" : "";
            ViewData["DescSortParm"] = sortOrder == "Desc" ? "desc_desc" : "Desc";
            ViewData["CatSortParm"] = sortOrder == "Cate" ? "Cate_desc" : "Cate";
            

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }


            ViewData["CurrentFilter"] = searchString;

            //var students = from s in list
            //               select s;
            if (!String.IsNullOrEmpty(searchString))
            {
                students = students.Where(s => s.Description.Contains(searchString)
                                       || s.NameCategory.Contains(searchString)
                                       || s.NameVariant.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "desc_desc":
                    students = students.OrderByDescending(s => s.Description);
                    break;
                case "Desc":
                    students = students.OrderBy(s => s.Description);
                    break;
                case "Cate":
                    students = students.OrderBy(s => s.NameCategory);
                    break;
                case "Cate_desc":
                    students = students.OrderByDescending(s => s.NameCategory);
                    break;              
                case "Var_desc":
                    students = students.OrderByDescending(s => s.NameVariant);
                    break;
                default:                   
                    students = students.OrderBy(s => s.NameVariant);
                    break;
            }

            int pageSize = 3;
            return View(await PaginatedList<VMVariant>.CreateAsync(students, pageNumber ?? 1, pageSize));
        }

        // GET: Variants/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            VMVariant abc = await (from v in _context.Variants
                                         join c in _context.Categories
                                         on v.IdCategory equals c.IdCategory
                                         select new VMVariant
                                         {
                                             IdVariant = v.IdVariant,
                                             NameVariant = v.NameVariant,
                                             Description = v.Description,
                                             NameCategory = c.NameCategory
                                         }).FirstOrDefaultAsync(m => m.IdVariant == id);
            if (id == null)
            {
                return NotFound();
            }

            var variant = await _context.Variants
                .FirstOrDefaultAsync(m => m.IdVariant == id);
            if (variant == null)
            {
                return NotFound();
            }

            return View(abc);
        }

        // GET: Variants/Create
        public IActionResult Create()
        {
            List<Category> cl = new List<Category>();
            cl = (from c in _context.Categories select c).ToList();
            cl.Insert(0, new Category { IdCategory = 0, NameCategory = "--Select Category Name--" ,Description=""});
            ViewBag.message = cl;
            return View();
        }

        // POST: Variants/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create  ([Bind("IdVariant,NameVariant,Description,IdCategory,IsDelete")] Variant variant)
        {
            if (ModelState.IsValid)
            {

                _context.Add(variant);
                await _context.SaveChangesAsync();
                
                return RedirectToAction(nameof(Index));
            }
            //List<Category> cl = new List<Category>();
            //cl = (from c in _context.Categories select c).ToList();
            //cl.Insert(0, new Category { IdCategory = 0, NameCategory = "--Select Category Name--", Description = "" });
            //ViewBag.message = cl;

            return View(variant);
        }

        // GET: Variants/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            List<Category> cl = new List<Category>();
            cl = (from c in _context.Categories select c).ToList();
            cl.Insert(0, new Category { IdCategory = 0, NameCategory = "--Select Category Name--",Description="" });
            ViewBag.message = cl;
            if (id == null)
            {
                return NotFound();
            }

            var variant = await _context.Variants.FindAsync(id);
            if (variant == null)
            {
                return NotFound();
            }
            return View(variant);
        }

        // POST: Variants/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdVariant,NameVariant,Description,IdCategory,IsDelete")] Variant variant)
        {
            if (id != variant.IdVariant)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(variant);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!VariantExists(variant.IdVariant))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(variant);
        }

        // GET: Variants/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var variant = await _context.Variants
                .FirstOrDefaultAsync(m => m.IdVariant == id);
            if (variant == null)
            {
                return NotFound();
            }

            return View(variant);
        }

        // POST: Variants/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id, [Bind("IdVariant,NameVariant,Description,IdCategory,IsDelete")] Variant variant)
        {
            if (id != variant.IdVariant)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                variant.IsDelete = true;
                try
                {
                    _context.Update(variant);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!VariantExists(variant.IdVariant))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(variant);
        }

        private bool VariantExists(int id)
        {
            return _context.Variants.Any(e => e.IdVariant == id);
        }
        
    }
}
