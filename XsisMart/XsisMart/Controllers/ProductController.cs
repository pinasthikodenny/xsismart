﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using XsisMart.Models;
using XsisMart.Utils;
using XsisMart.ViewModels;

namespace XsisMart.Controllers
{
    public class ProductController : Controller
    {
        private readonly XsisMartContextModel _context;
        private readonly IWebHostEnvironment abc;

        public ProductController(XsisMartContextModel context, IWebHostEnvironment webHostEnvironment)
        {
            _context = context;
            abc = webHostEnvironment;
        }
        public async Task<IActionResult> Index(string sortOrder,
            string currentFilter,
            string searchString,
            int? pageNumber, int? pSize)
        {
            var students = from p in _context.Products
                                          join v in _context.Variants
                                          on p.IdVariant equals v.IdVariant
                                          join c in _context.Categories
                                          on v.IdCategory equals c.IdCategory
                                          select new VMProduct
                                          {
                                              IdProduct = p.IdProduct,
                                              NameProduct = p.NameProduct,
                                              Price = p.Price,
                                              Stock = p.Stock,
                                              NameCategory = c.NameCategory,
                                              NameVariant = v.NameVariant,
                                              Image=p.Image
                                          }
                                          ;

            ViewData["CurrentSort"] = sortOrder;
            ViewData["CurrentpSize"] = pSize;
            ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewData["PriceSortParm"] = sortOrder == "Price" ? "price_desc" : "Price";
            ViewData["CatSortParm"] = sortOrder == "Cate" ? "Cate_desc" : "Cate";
            ViewData["VarSortParm"] = sortOrder == "Var" ? "Var_desc" : "Var";
            ViewBag.PageSize = PaginatedList<VMProduct>.PageSizeList();

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }


            ViewData["CurrentFilter"] = searchString;

            //var students = from s in list
            //               select s;
            if (!String.IsNullOrEmpty(searchString))
            {
                students = students.Where(s => s.NameProduct.Contains(searchString)
                                       || s.NameCategory.Contains(searchString)
                                       || s.NameVariant.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "name_desc":
                    students = students.OrderByDescending(s => s.NameProduct);
                    break;
                case "Price":
                    students = students.OrderBy(s => s.Price);
                    break;
                case "price_desc":
                    students = students.OrderByDescending(s => s.Price);
                    break;
                case "Cate":
                    students = students.OrderBy(s => s.NameCategory);
                    break;
                case "Cate_desc":
                    students = students.OrderByDescending(s => s.NameCategory);
                    break;
                case "Var":
                    students = students.OrderBy(s => s.NameVariant);
                    break;
                case "Var_desc":
                    students = students.OrderByDescending(s => s.NameVariant);
                    break;
                default:
                    students = students.OrderBy(s => s.NameProduct);
                    break;
            }
            int pageSize = pSize??5;
            
            return View(await PaginatedList<VMProduct>.CreateAsync(students, pageNumber ?? 1, pageSize) );
            //return View(students);
        }
        public  IActionResult Create()
        {
            List<Category> cd = new List<Category>();
            cd = (from c in _context.Categories select c).ToList();
            cd.Insert(0, new Category { IdCategory = 0, NameCategory = "--Select Category Name--", Description = "" });
            ViewBag.klm = cd;

            //List<Category>ListCategory = await DataCategory();
            List<Variant> cl = new List<Variant>();
            cl.Insert(0, new Variant { IdVariant = 0, NameVariant = "--Select Variant Name--", Description = "", IsDelete = false, IdCategory = 0 });

            //ViewBag.abc = ListCategory;
            ViewBag.ListVariant = cl;

            return View();

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(VMProduct _product)
        {

            if (ModelState.IsValid)
            {
                Product prodNew = new Product();
                prodNew.NameProduct = _product.NameProduct;
                prodNew.Stock = _product.Stock;
                prodNew.Price = _product.Price;
                prodNew.IdVariant = _product.IdVariant;
                prodNew.Image = _product.Image;

                _context.Add(prodNew);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            List<Category> cd = new List<Category>();
            cd = (from c in _context.Categories select c).ToList();
            cd.Insert(0, new Category { IdCategory = 0, NameCategory = "--Select Category Name--", Description = "" });
            ViewBag.klm = cd;

            //List<Category>ListCategory = await DataCategory();
            List<Variant> cl = new List<Variant>();
            cl.Insert(0, new Variant { IdVariant = 0, NameVariant = "--Select Variant Name--", Description = "", IsDelete = false, IdCategory = 0 });

            //ViewBag.abc = ListCategory;
            ViewBag.ListVariant = cl;


            return View(_product);
        }
        public async Task<IActionResult> Detail(int _IdProduct)
        {
            VMProduct list = await (from p in _context.Products
                                    join v in _context.Variants
                                    on p.IdVariant equals v.IdVariant
                                    join c in _context.Categories
                                    on v.IdCategory equals c.IdCategory
                                    select new VMProduct
                                    {
                                        IdProduct = p.IdProduct,
                                        NameProduct = p.NameProduct,
                                        Price = p.Price,
                                        Stock = p.Stock,
                                        NameCategory = c.NameCategory,
                                        NameVariant = v.NameVariant,
                                        Image=p.Image
                                    }
                                          ).FirstOrDefaultAsync(h => h.IdProduct == _IdProduct);
            if (_IdProduct == null)
            {
                return NotFound();
            }
            Product product = await _context.Products.FirstOrDefaultAsync(h => h.IdProduct == _IdProduct);
            if (product == null)
            {
                return NotFound();
            }
            return View(list);
        }
        public async Task<IActionResult> Edit(int id)
        {
            List<Category> cd = new List<Category>();
            cd = (from c in _context.Categories select c).ToList();
            cd.Insert(0, new Category { IdCategory = 0, NameCategory = "--Select Category Name--", Description = "" });
            ViewBag.klm = cd;



            List<Variant> cl = new List<Variant>();
            cl = (from d in _context.Variants select d).ToList();
            cl.Insert(0, new Variant { IdVariant = 0, NameVariant = "--Select Variant Name--", Description = "", IsDelete = false, IdCategory = 0 });

            ViewBag.ListVariant = cl;

            //ViewBag.klm = await DataCategory();


            if (id == null)
            {
                return NotFound();
            }
            VMProduct product = await DataVM(id);//buat dapetin data produk(make VM karena butuh joinan kalo gapake pakenya model)

            //var product = await _context.Products.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }
            ViewBag.ListVariant = await DataVariant(product.IdCategory);
            return View(product);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Product _product)
        {
            if (ModelState.IsValid)
            {
                Product prodNew = new Product();
                prodNew.NameProduct = _product.NameProduct;
                prodNew.Stock = _product.Stock;
                prodNew.Price = _product.Price;
                prodNew.IdVariant = _product.IdVariant;
                prodNew.Image = _product.Image;

                _context.Update(_product);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            List<Category> cd = new List<Category>();
            cd = (from c in _context.Categories select c).ToList();
            cd.Insert(0, new Category { IdCategory = 0, NameCategory = "--Select Category Name--", Description = "" });
            ViewBag.klm = cd;

            //List<Category>ListCategory = await DataCategory();
            List<Variant> cl = new List<Variant>();
            cl.Insert(0, new Variant { IdVariant = 0, NameVariant = "--Select Variant Name--", Description = "", IsDelete = false, IdCategory = 0 });

            //ViewBag.abc = ListCategory;
            ViewBag.ListVariant = cl;

            return View(_product);
        }

        //public async Task<IActionResult> Delete(int id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }
        //    Product product = await _context.Products.FirstOrDefaultAsync(l => l.IdProduct == id);
        //    if (ModelState.IsValid)
        //    {
        //        _context.Remove(product);
        //        await _context.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }
        //    return View(product);
        //}
        public async Task<IActionResult> Delete(int id)
        {
            if (id == null)
            {
                return NotFound();
            }
            Product product = await _context.Products.FirstOrDefaultAsync(l => l.IdProduct == id);
            return View(product);
        }
        [HttpPost]
        public async Task<IActionResult> Delete(Product _product)
        {
            if (ModelState.IsValid)
            {
                _context.Remove(_product);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(_product);
        }
        public async Task<List<Category>> DataCategory()
        {
            List<Category> cd = await _context.Categories.ToListAsync();
            cd.Insert(0, new Category { IdCategory = 0, NameCategory = "--Select Category Name--" });
            ViewBag.klm = cd;
            return cd;
        }
        public async Task<List<Variant>> DataVariant(int IdCategory)
        {
            List<Variant> list = await _context.Variants.Where(a => a.IdCategory.Equals(IdCategory)).ToListAsync();
            list.Insert(0, new Variant { IdVariant = 0, NameVariant = "--Select variant Name--" });
            return list;
        }
        public async Task<JsonResult> GetDataVariantAjax(int IdCategory)
        {
            List<Variant> list = await DataVariant(IdCategory);
            return Json(list);
        }
        public async Task<VMProduct> DataVM(int _IdProduct)
        {
            VMProduct list = await (from p in _context.Products
                                    join v in _context.Variants
                                    on p.IdVariant equals v.IdVariant
                                    join c in _context.Categories
                                    on v.IdCategory equals c.IdCategory
                                    select new VMProduct
                                    {
                                        IdProduct = p.IdProduct,
                                        NameProduct = p.NameProduct,
                                        Price = p.Price,
                                        Stock = p.Stock,
                                        NameCategory = c.NameCategory,
                                        NameVariant = v.NameVariant,
                                        IdCategory = c.IdCategory,
                                        IdVariant = v.IdVariant,
                                        Image=p.Image
                                    }
                                         ).FirstOrDefaultAsync(h => h.IdProduct == _IdProduct);
            return list;
        }
        [HttpPost]
        public IActionResult OnPostMyUploader(IFormFile MyUploader)
        {
            if (MyUploader != null)
            {
                var fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(MyUploader.FileName);
                string uploadsFolder = Path.Combine(abc.WebRootPath, "mediaUpload");
                string filePath = Path.Combine(uploadsFolder, fileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    MyUploader.CopyTo(fileStream);
                }
                return Json(new { status = "success", fileName = fileName });
            }
            return new ObjectResult(new { status = "fail" });

        }

    }
}
