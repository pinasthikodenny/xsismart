﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using XsisMart.Models;
using XsisMart.ViewModels;

namespace XsisMart.Controllers
{
    public class TransactionController : Controller
    {
        private readonly XsisMartContextModel _context;

        public TransactionController(XsisMartContextModel context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddCart(VMTransaction trx)
        {
            try
            {
                OrderHeader header = new OrderHeader();
                OrderDetail detail = new OrderDetail();
                DateTime dt = DateTime.Now;
                int d = dt.Day;
                int m = dt.Month;
                int y = dt.Year;
                string CodeTrx = $"XM-{d}-{m}-{y}-0001";
                string LastCode = await LastCodeTransaction();

                if (LastCode != "")
                {
                    string lastString = LastCode.Substring(LastCode.Length - 4, 4);
                    int lastInt = Convert.ToInt32(lastString) + 1;
                    string joinString = "0000" + lastInt.ToString();
                    CodeTrx = $"XM-{d}-{m}-{y}-" + joinString.Substring(joinString.Length - 4, 4);
                }
                header = await _context.OrderHeaders.Where(a => a.IdCustomer == 1 && a.IsCheckout == false).FirstOrDefaultAsync();

                if (header == null)
                {
                    header = new OrderHeader();
                    header.CodeTransaction = CodeTrx;
                    //header.IdCustomer = trx.IdCustomer;
                    header.IsCheckout = false;
                    header.IdCustomer = 1;
                    _context.Add(header);
                    await _context.SaveChangesAsync();
                }

                detail.IdProduct = trx.IdProduct;
                detail.IdHeader = header.IdHeader;
                detail.Quantity = trx.Quantity;
                detail.Amount = trx.Amount;
                _context.Add(detail);
                await _context.SaveChangesAsync();

                return Json(new { status = "success" });
            }
            catch (Exception e)
            {
                return Json(new { status = "error", message = e.Message.ToString() });
            }
        }

        public async Task<string> LastCodeTransaction()
        {
            string lastNoTrx = "";
            OrderHeader data = await _context.OrderHeaders.
                                     OrderByDescending(a => a.CodeTransaction).FirstOrDefaultAsync();
            if (data != null)
            {
                lastNoTrx = data.CodeTransaction;
            }
            return lastNoTrx;
        }
        
    }
}
