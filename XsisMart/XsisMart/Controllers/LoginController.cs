﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Threading.Tasks;
using XsisMart.Models;
using XsisMart.ViewModels;

namespace XsisMart.Controllers
{
    public class LoginController : Controller
    {
        private readonly XsisMartContextModel _context;

        public LoginController(XsisMartContextModel context)
        {
            _context = context;
        }
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Login(Customer mnm)
        {
            var user = _context.Customers.FirstOrDefault(u => u.Email == mnm.Email);
            var isPasswordMatched = VerifyPassword(mnm.Password, user.StoredSalt, user.Password);

            if (isPasswordMatched)
            {
                HttpContext.Session.SetString("customername", user.NameCustomer);
                HttpContext.Session.SetInt32("Idcustomer", user.IdCustomer);
                return RedirectToAction("Index", "Home");
            }

            return View();
        }
        public IActionResult LogOut()
        {
            return RedirectToAction("Login");
        }
        private Customer CustExist(string email, string password)
        {
            return _context.Customers.FirstOrDefault(e => e.Email == email && e.Password == password);
        }
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(VMCustomer cust)
        {
            if (ModelState.IsValid)
            {
                Customer cus = new Customer();
                cus.NameCustomer = cust.NameCustomer;
                cus.IdCustomer = cust.IdCustomer;
                cus.Password = cust.Password;
                cus.Email = cust.Email;
                cus.Address = cust.Address;

                var hashsalt = EncryptPassword(cus.Password);
                cus.Password = hashsalt.Hash;
                cus.StoredSalt = hashsalt.Salt;

                _context.Add(cus);
                await _context.SaveChangesAsync();
                return RedirectToAction("Login");
            }
            return View(cust);
        }

        public IActionResult SendEmail(VMCustomer cust)

        {
            Random rnd = new Random();
            string otp = rnd.Next(1000, 9999).ToString();
            cust.Otp = otp;

            SmtpClient client = new SmtpClient("smtp.gmail.com", 587);

            client.EnableSsl = true;

            MailAddress from = new MailAddress("tupakshakur12345@gmail.com", "[Tupakshakurtherapper]");

            MailAddress to = new MailAddress(cust.Email);

            MailMessage message = new MailMessage(from, to);

            message.Body = "Your OTP code is "+otp;

            message.Subject = "OTP Code";

            NetworkCredential myCreds = new NetworkCredential("tupakshakur12345@gmail.com", "udingambut", "");

            client.Credentials = myCreds;

            try

            {

                client.Send(message);
                return Json(new { status = "success", data = cust });

            }

            catch (Exception )

            {

                return Json(new { status = "error"});

            }          

        }
        public IActionResult VerifyOTP(VMCustomer cust)
        {
            return View(cust);
        }
        public HashSalt EncryptPassword(string password)
        {
            byte[] salt = new byte[128 / 8]; // Generate a 128-bit salt using a secure PRNG
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }
            string encryptedPassw = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8
            ));
            return new HashSalt { Hash = encryptedPassw, Salt = salt };
        }

        public bool VerifyPassword(string enteredPassword, byte[] salt, string storedPassword)
        {
            string encryptedPassw = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: enteredPassword,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8
            ));
            return encryptedPassw == storedPassword;
        }
        public string ValidateEmailId(string emailId)
        {
            var emailStatus = _context.Customers.Where(m => m.Email == emailId).FirstOrDefault();
            if (emailStatus != null)
            {
                return "1";
            }
            else
            {
                return "0";
            }
        }

    }
}
