﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using XsisMart.Models;
using XsisMart.Utils;

namespace XsisMart.Controllers
{
    public class CategoriesController : Controller
    {
        private readonly XsisMartContextModel _context;
        //private readonly IWebHostEnvironment abc;

        public CategoriesController(XsisMartContextModel context/*, IWebHostEnvironment webHostEnvironment*/)
        {
            _context = context;
            //abc = webHostEnvironment;

        }

        // GET: Categories
        public async Task<IActionResult> Index (string sortOrder,
            string currentFilter,
            string searchString,
            int? pageNumber, int? pSize)
        {
            ViewData["CurrentpSize"] = pSize;
            ViewData["CurrentSort"] = sortOrder;
            ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewData["DescSortParm"] = sortOrder == "Desc" ? "desc_desc" : "Desc";
            ViewBag.PageSize = PaginatedList<Category>.PageSizeList();

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewData["CurrentFilter"] = searchString;
            var students = from s in _context.Categories
                           select s;

            if (!String.IsNullOrEmpty(searchString))
            {
                students = students.Where(s => s.NameCategory.Contains(searchString)
                                       || s.Description.Contains(searchString));
            }

            switch (sortOrder)
            {
                case "name_desc":
                    students = students.OrderByDescending(s => s.NameCategory);
                    break;
                case "Desc":
                    students = students.OrderBy(s => s.Description);
                    break;
                case "desc_desc":
                    students = students.OrderByDescending(s => s.Description);
                    break;
                default:
                    students = students.OrderBy(s => s.NameCategory);
                    break;
            }

            int pageSize = pSize ?? 5;
            return View(await PaginatedList<Category>.CreateAsync(students, pageNumber ?? 1, pageSize));
            //return View(await students.AsNoTracking().ToListAsync());
        }

        // GET: Categories/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var category = await _context.Categories
                .FirstOrDefaultAsync(m => m.IdCategory == id);
            if (category == null)
            {
                return NotFound();
            }

            return View(category);
        }

        // GET: Categories/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Categories/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdCategory,NameCategory,Description,Image")] Category category)
        {
            if (ModelState.IsValid)
            {
                _context.Add(category);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(category);
        }

        // GET: Categories/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var category = await _context.Categories.FindAsync(id);
            if (category == null)
            {
                return NotFound();
            }
            return View(category);
        }

        // POST: Categories/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdCategory,NameCategory,Description")] Category category)
        {
            if (id != category.IdCategory)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(category);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CategoryExists(category.IdCategory))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(category);
        }

        // GET: Categories/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var category = await _context.Categories
                .FirstOrDefaultAsync(m => m.IdCategory == id);
            if (category == null)
            {
                return NotFound();
            }

            return View(category);
        }

        // POST: Categories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var category = await _context.Categories.FindAsync(id);
            _context.Categories.Remove(category);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CategoryExists(int id)
        {
            return _context.Categories.Any(e => e.IdCategory == id);
        }    
       //[HttpPost]
       // public IActionResult OnPostMyUploader(IFormFile MyUploader)
       // {
       //     if (MyUploader != null)
       //     {
       //         var fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(MyUploader.FileName);
       //         string uploadsFolder = Path.Combine(abc.WebRootPath, "mediaUpload");                
       //         string filePath = Path.Combine(uploadsFolder, fileName);
       //         using (var fileStream = new FileStream(filePath, FileMode.Create))
       //         {
       //             MyUploader.CopyTo(fileStream);
       //         }
       //         return Json(new { status = "success",fileName=fileName  });
       //     }
       //     return new ObjectResult(new { status = "fail" });

       // }
    }

}
