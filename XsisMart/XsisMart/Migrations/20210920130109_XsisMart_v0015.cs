﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace XsisMart.Migrations
{
    public partial class XsisMart_v0015 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte[]>(
                name: "StoredSalt",
                table: "Customers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StoredSalt",
                table: "Customers");
        }
    }
}
