﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace XsisMart.Migrations
{
    public partial class XsisMart_v004 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IdProduct",
                table: "Variants");

            migrationBuilder.AddColumn<int>(
                name: "IdCategory",
                table: "Variants",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IdCategory",
                table: "Variants");

            migrationBuilder.AddColumn<int>(
                name: "IdProduct",
                table: "Variants",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
