﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace XsisMart.Models
{
    public class Variant
    {
        public int IdVariant { get; set; }
        [Required]
        public string NameVariant { get; set; }
        [Required]
        public string Description { get; set; }
        public int IdCategory { get; set; }
        public bool? IsDelete { get; set; }
    }
}
