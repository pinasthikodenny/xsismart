﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace XsisMart.Models
{
    public class Category
    {
        //idcategory,namecategory,description
        public int IdCategory { get; set; }
        [MaxLength(100)]
        [Required]
        public string NameCategory { get; set; }
        [Required]
        public string Description { get; set; }

    }
}
