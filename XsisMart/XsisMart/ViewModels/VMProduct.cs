﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace XsisMart.ViewModels
{
    public class VMProduct
    {
        public int IdProduct { get; set; }
        [Required]
        public string NameProduct { get; set; }
        [Required]
        public decimal Price { get; set; }
        [Required]
        public int Stock { get; set; }
        public int IdVariant { get; set; }
        public string NameVariant { get; set; }
        public int IdCategory { get; set; }
        public string NameCategory { get; set; }
        public string Image { get; set; }
    }
}
