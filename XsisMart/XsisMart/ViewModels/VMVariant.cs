﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace XsisMart.ViewModels
{
    public class VMVariant
    {
        public int IdVariant { get; set; }
        public string NameVariant { get; set; }
        public string Description { get; set; }
        public bool? IsDelete { get; set; }
        public int IdCategory { get; set; }
        public string NameCategory { get; set; }
    }
}
